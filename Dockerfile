FROM ubuntu:latest
RUN apt-get update
RUN apt-get -y install wget
RUN mkdir /xmrig
RUN wget https://github.com/xmrig/xmrig/releases/download/v5.6.0/xmrig-5.6.0-xenial-x64.tar.gz
RUN tar -xvf xmrig-5.6.0-xenial-x64.tar.gz -C /xmrig
WORKDIR /xmrig/xmrig-5.6.0
CMD ./xmrig -o $MINING_POOL -u $WALLET_ADDRESS.$WORKER_NAME

